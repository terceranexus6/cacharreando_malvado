# cacharreando para el mal
## Hardware hacking

En este repo meto todo lo necesario para ir preparado al taller de Hardware Hacking con open hardware de la h-con el día 31 de enero de 2020. El repo tiene licencia libre, así que si te apetece puedes hacerte un fork y añadir cosas, te invito a que mandes **Merge request** incluso, si quieres. 


Algunos de las placas que usamos en el taller son:

- Arduino nano
- Attiny85
- RPI 3, 4 o zero
- FONA 808 GSM + GPS Breakout/ GPS
- RFID lector/escritor para arduino
- Mencionamos: [HUZZAH32 – ESP32 Feather Board](https://learn.adafruit.com/adafruit-huzzah32-esp32-feather/overview)

Especial mención al proyecto relativo a la PI ZERO, el [pwnagotchi](https://pwnagotchi.ai/) desarrollado por  [evilsocket](https://wwww.evilsocket.net), un grande, ¡muchas gracias!

## Apartados del taller:

- Introducción al hardware hacking libre
- Introducción a electrónica
- Configuración del IDE
- Setup básico de un Attiny85 paso a paso
- RFID, cómo funciona, qué podemos hacer
- FONA o GPS geofencing 
- Otros proyectos + RPI y pwnagotchi

## Board manager en json

http://arduino.esp8266.com/stable/package_esp8266com_index.json

https://raw.githubusercontent.com/wiki/tobozo/Arduino/package_deauther_index.json

http://digistump.com/package_digistump_index.json

## Librerias

mqtt, GSM, keyboard, wifi, adafruit fona library,  sleepydog, attinyserialout, espsoftwareserial

